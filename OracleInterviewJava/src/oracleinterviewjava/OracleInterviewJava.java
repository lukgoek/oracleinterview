
package oracleinterviewjava;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class OracleInterviewJava {
    private String url = "jdbc:oracle:thin:@localhost:1521/XEPDB1";
    private String username = "SYSTEM";
    private String password = "admin";
    
    private String filePath = "C:\\Users\\lugoe\\Desktop\\Oracle_Interview_Results.txt";
    private String result = "";
    
    /**
    *Returns the connection object to the db to be used after
    * in the execution
    * 
    */
   private Connection dbConnection(){
       Connection con = null;
        try{
            con = DriverManager.getConnection(this.url, this.username, this.password);
            return con;
       }catch(SQLException ex){
            ex.printStackTrace();
       } 
       return con;
   }
   
   
   /**
    * This method has no returns just finish the connection with the db object received
    * an prints a message regarding it
    * 
    * @param con A connection object pointing to a db
    */
   private void closeDB(Connection con){
        try{
            con.close();
            //System.out.println("DB closed.");
        }catch(Exception ex){
                ex.printStackTrace();
        }
            
        
   }
   
   
   /**
    *This method created a statement with the db connection object
    * to execute a query after it on this case we are doing a select 
    * and saving the data in the global variable "result"
    */
   private void getData(){
       Connection dbCon = dbConnection();
       if(dbCon == null){
           return;
       }
       
       Statement stmt;
       
       String query = "SELECT \n" +
                        "    A.countryCode AS \"Country Code\", \n" +
                        "    E.employeeName AS \"Employee Name\", \n" +
                        "    EE.employeeName AS \"Manager Name\"  \n" +
                        "FROM employee E \n" +
                        "LEFT OUTER JOIN employee EE ON EE.employeeId = E.mgrid\n" +
                        "INNER JOIN address A ON a.addressid = e.addressid\n" +
                        "ORDER BY A.countryCode DESC";
        
       try{
            stmt = dbCon.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            String lastCountryCode = "";
            
            while(rs.next()){
                String  countryCode = rs.getString("Country Code");
                String  employeeName = rs.getString("Employee Name");
                String  manager = rs.getString("Manager Name");
                
               
                //if lastCountryCode is equals to the contryCode returned by a row
                //in the query results we are changing it to "" 
                if(countryCode.equals(lastCountryCode)){
                    countryCode = "";
                }else{//if it is not equals that mean that we can show it for 1 time
                    lastCountryCode = countryCode;
                }
                
                //to avoid showing a null manager just chaged it to ""
                if(manager ==  null){
                    manager="";
                }
                
                //finally we save the data in the requested format
                result += countryCode+"\t"+employeeName+"\t"+manager+"\n";
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
           closeDB(dbCon);
       }
       
   }
   
   
   /**
    * This method creates a file in the destination and with the extension
    * that user puts on the path variable
    * 
    * @param path contains a String with the path for the file and the name.extension of the file
    * @param result String that contains what will be written on the result file
    */
   private void createFile(String path, String result){
       try{
        File file = new File(path);
        
        //check if the file exists 
        if(!file.exists()){
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(result);
        bw.close();
        
       }catch(Exception ex){
           ex.printStackTrace();
       }
   }
   
   
    public static void main(String[] args) {
        OracleInterviewJava obj = new OracleInterviewJava();
        obj.dbConnection();
        obj.getData();
        obj.createFile(obj.filePath, obj.result);
       
    }
    
}
